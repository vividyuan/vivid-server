import request from "@/utils/request";

export function login(data){
    return request({
        url: 'sys/auth/login',
        method: "POST",
        data: data
    })
}