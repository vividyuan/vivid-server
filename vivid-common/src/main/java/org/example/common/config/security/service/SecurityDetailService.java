package org.example.common.config.security.service;

import cn.hutool.core.util.ObjectUtil;
import org.example.common.domain.entity.UmsMenu;
import org.example.common.domain.entity.UmsRole;
import org.example.common.domain.entity.UmsSysUser;
import org.example.common.domain.vo.LoginUserVo;
import org.example.common.mapper.UmsMenuMapper;
import org.example.common.mapper.UmsSysUserMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SecurityDetailService implements UserDetailsService {

    private final UmsSysUserMapper umsSysUserMapper;
    private final UmsMenuMapper umsMenuMapper;

    public SecurityDetailService(UmsSysUserMapper umsSysUserMapper, UmsMenuMapper umsMenuMapper) {
        this.umsSysUserMapper = umsSysUserMapper;
        this.umsMenuMapper = umsMenuMapper;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        int accountType = 0;
        UmsSysUser sysUser = umsSysUserMapper.selectByUserName(username,accountType);
        if(ObjectUtil.isNotNull(sysUser)){
            List<UmsRole> roleList = sysUser.getRoleList();
            System.out.println("roleList=======>00"+roleList);
            List<Long> roleIds = roleList.stream().map(item -> item.getRoleId()).collect(Collectors.toList());
            List<UmsMenu> menuList = umsMenuMapper.selectByRoleIds(roleIds);
            System.out.println("menuList========>"+menuList);

        }
        LoginUserVo loginUserVo = new LoginUserVo();
        loginUserVo.setUmsSysUser(sysUser);
        loginUserVo.setLoginTime(sysUser.getId());

        return loginUserVo;
    }
}
