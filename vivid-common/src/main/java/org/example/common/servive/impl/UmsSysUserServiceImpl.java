package org.example.common.servive.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.example.common.domain.entity.UmsSysUser;
import org.example.common.mapper.UmsSysUserMapper;
import org.example.common.servive.IUmsSysUserService;
import org.springframework.stereotype.Service;

@Service
public class UmsSysUserServiceImpl extends ServiceImpl<UmsSysUserMapper, UmsSysUser> implements IUmsSysUserService {
}
