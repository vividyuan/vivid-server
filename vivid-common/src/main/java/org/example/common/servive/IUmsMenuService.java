package org.example.common.servive;

import com.baomidou.mybatisplus.extension.service.IService;
import org.example.common.domain.entity.UmsMenu;

public interface IUmsMenuService extends IService<UmsMenu> {
}
