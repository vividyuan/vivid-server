package org.example.common.servive;

import com.baomidou.mybatisplus.extension.service.IService;
import org.example.common.domain.entity.UmsRole;

public interface IUmsRoleService extends IService<UmsRole> {
}
