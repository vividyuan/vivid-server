package org.example.common.servive.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.example.common.domain.entity.UmsRole;
import org.example.common.mapper.UmsRoleMapper;
import org.example.common.servive.IUmsRoleService;
import org.springframework.stereotype.Service;

@Service
public class UmsRoleServiceImpl extends ServiceImpl<UmsRoleMapper, UmsRole> implements IUmsRoleService {
}
