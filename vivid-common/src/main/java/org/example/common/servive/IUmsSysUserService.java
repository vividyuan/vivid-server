package org.example.common.servive;

import com.baomidou.mybatisplus.extension.service.IService;
import org.example.common.domain.entity.UmsSysUser;

public interface IUmsSysUserService extends IService<UmsSysUser> {
}
