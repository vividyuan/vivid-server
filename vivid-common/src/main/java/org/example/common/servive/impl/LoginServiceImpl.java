package org.example.common.servive.impl;

import org.example.common.domain.dto.LoginDto;
import org.example.common.domain.vo.LoginUserVo;
import org.example.common.servive.ILoginService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements ILoginService {

    private final AuthenticationManager authenticationManager;

    public LoginServiceImpl(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public LoginUserVo login(LoginDto loginDto) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
                = new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword());
        Authentication authenticate = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        LoginUserVo loginVo = (LoginUserVo) authenticate.getPrincipal();
        System.out.println("登录时找到的用户========>"+loginVo.getUmsSysUser());
        return null;
    }
}
