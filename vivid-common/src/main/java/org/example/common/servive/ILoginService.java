package org.example.common.servive;

import org.example.common.domain.dto.LoginDto;
import org.example.common.domain.vo.LoginUserVo;

public interface ILoginService {
    LoginUserVo login(LoginDto loginDto);
}
