package org.example.common.servive.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.example.common.domain.entity.UmsMenu;
import org.example.common.mapper.UmsMenuMapper;
import org.example.common.servive.IUmsMenuService;
import org.springframework.stereotype.Service;

@Service
public class UmsMenuServiceImpl extends ServiceImpl<UmsMenuMapper, UmsMenu> implements IUmsMenuService {
}