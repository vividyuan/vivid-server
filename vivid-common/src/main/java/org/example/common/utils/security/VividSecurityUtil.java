package org.example.common.utils.security;

import cn.hutool.core.util.ObjectUtil;
import org.example.common.constants.HttpStatus;
import org.example.common.domain.vo.LoginUserVo;
import org.example.common.exeption.ServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class VividSecurityUtil {
    public static Authentication getAuthentication(){
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public static LoginUserVo getLoginUser(){
        return (LoginUserVo) getAuthentication().getPrincipal();
    }

    public static Long getUserId(){
        Long userId = getLoginUser().getId();
        if(ObjectUtil.isNull(userId)){
            throw new ServiceException(HttpStatus.FORBIDDEN,"");
        }

        return userId;
    }

    public static String getUsername(){
        String username = getLoginUser().getUsername();
        if(ObjectUtil.isNull(username)){
            throw new ServiceException(HttpStatus.FORBIDDEN,"");
        }
        return username;
    }
}
