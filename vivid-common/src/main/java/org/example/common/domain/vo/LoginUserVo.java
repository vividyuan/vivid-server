package org.example.common.domain.vo;

import cn.hutool.core.util.ObjectUtil;
import lombok.Data;
import org.example.common.domain.entity.UmsSysUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class LoginUserVo implements UserDetails {

    private  Long id;
    private  UmsSysUser umsSysUser = new UmsSysUser();
    private  String token;
    private  long loginTime;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<String> perms = umsSysUser.getPerms();
        // 判空，返回数据
        if(ObjectUtil.isNotEmpty(perms)) {
            return perms.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public String getPassword() {
        return umsSysUser.getPassword();
    }

    @Override
    public String getUsername() {
        return umsSysUser.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
