package org.example.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.example.common.domain.entity.UmsRole;

@Mapper
public interface UmsRoleMapper extends BaseMapper<UmsRole> {
}
