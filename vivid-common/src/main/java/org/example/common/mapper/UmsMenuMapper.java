package org.example.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.example.common.domain.entity.UmsMenu;

import java.util.List;

@Mapper
public interface UmsMenuMapper extends BaseMapper<UmsMenu> {
    List<UmsMenu> selectByRoleIds(@Param("roleIds") List<Long> roleIds);
}
