package org.example.module.sys.controller;

import org.example.common.domain.dto.LoginDto;
import org.example.common.domain.vo.LoginUserVo;
import org.example.common.response.DataResult;
import org.example.common.servive.ILoginService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("sys/auth")
public class LoginController {

    private final ILoginService loginService;

    public LoginController(ILoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("/login")
    public DataResult Login(@RequestBody LoginDto loginDto){
        LoginUserVo loginUserVo = loginService.login(loginDto);
        return DataResult.success();
    }
}
