package org.example.module.sys.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("sys/user")
public class UserController {

    @RequestMapping("/test")
    public String test(){
        return "HELLO WORLD!";
    }
}
